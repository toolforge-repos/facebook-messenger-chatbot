SUBSCRIBED_SUBTITLE_TEXT = {
	"en" => "Subscribed  ✔️",
	"ta" =>	"குழுசேர்ந்துள்ளீர்கள் ✔️"
}


UNSUBSCRIBED_SUBTITLE_TEXT = {
	"en" => "Unsubscribed ❌",
	"ta" => "குழுவிலக்கப்பட்டுள்ளீர்கள் ❌"
}

SUBSCRIPTION_TEXT = {
	"en" => "My Subscriptions",
	"ta" => "என் சந்தாக்கள்"
}
UNSUBSCRIBE_BUTTON = {
	"en" => "Unsubscribe",
	"ta" => "குழுவிலகல்"
}
SUBSCRIBE_BUTTON = {
	"en" =>  "Subscribe",
	"ta" => "பதிவு"
}

SUBSCRIBED_FEATURED_ARTICLE_TEXT ={
	"en" => "Thank you for subscribing, I'll send you Featured article daily !",
	"ta" => "சந்தாவிற்கு நன்றி, நான் தினசரி சிறப்பு கட்டுரை அனுப்புவேன்!"
}

UNSUBSCRIBED_FEATURED_ARTICLE_TEXT ={
	"en" => "You are successfully unsubscribed to Featured article! To get Featured article daily, do subscribe!",
	"ta" => "நீங்கள் வெற்றிகரமாக குழுவிலக்கப்பட்டுள்ளீர்கள்! தினசரி சிறப்பு கட்டுரை பெற, பதிவு செய்க!"
}

SUBSCRIBED_IMAGE_OF_THE_DAY_TEXT ={
	"en" => "Thank you for subscribing, I'll send you Image of the day daily !",
	"ta" => "சந்தாவிற்கு நன்றி, நான் தினசரி இன்றைய சிறப்பு படம் அனுப்புவேன்!"
}

UNSUBSCRIBED_IMAGE_OF_THE_DAY_TEXT ={
	"en" => "You are successfully unsubscribed to Image of the day! To get Image of the day content daily, do subscribe!",
	"ta" => "நீங்கள் வெற்றிகரமாக குழுவிலக்கப்பட்டுள்ளீர்கள்! தினசரி இன்றைய சிறப்பு படம் பெற, பதிவு செய்க!"
}

SUBSCRIBED_NEWS_TEXT ={
	"en" => "Thank you for subscribing, I'll send you Wiki news daily !",
	"ta" => "சந்தாவிற்கு நன்றி, நான் தினசரி செய்திகள் அனுப்புவேன்!"
}

UNSUBSCRIBED_NEWS_TEXT ={
	"en" => "You are successfully unsubscribed to News! To get news daily, do subscribe!",
	"ta" => "நீங்கள் வெற்றிகரமாக குழுவிலக்கப்பட்டுள்ளீர்கள்! தினசரி செய்திகள் பெற, பதிவு செய்க!"
}

SUBSCRIBED_ON_THIS_DAY_TEXT ={
	"en" => "Thank you for subscribing, I'll send you On this day daily !",
	"ta" => "சந்தாவிற்கு நன்றி, நான் தினசரி 'இன்றைய நாளில் கட்டுரை' அனுப்புவேன்!"
}

UNSUBSCRIBED_ON_THIS_DAY_TEXT ={
	"en" => "You are successfully unsubscribed to On this day! To get On this day articles daily, do subscribe!",
	"ta" => "நீங்கள் வெற்றிகரமாக குழுவிலக்கப்பட்டுள்ளீர்கள்! தினசரி 'இன்றைய நாளில் கட்டுரை' பெற, பதிவு செய்க!"
}
