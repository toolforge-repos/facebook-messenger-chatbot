class MessengerBot

	GREETING_MESSAGE = {
		"en" => "Hey!  Welcome to Wikipedia.",
		"ta" => "வணக்கம்! விக்கிப்பீடியா உங்களை வரவேற்கிறது"
	}

	QUICK_REPLY_HEADER = {
		"en" => "How can I help you?",
		"ta" => "நான் உங்களுக்கு ஏவ்வாறு உதவ வேண்டும் "
	}

	QUICK_REPLY_OPTIONS = {
		"Random Article" => {
			"en" => "Random Article",
			"ta" => "ஏதாவது ஒரு கட்டுரை"
		},
		"Most Read" => {
			"en" => "Most Read",
			"ta" => "அதிகம் வாசித்தது"
		},
		"Featured Article" => {
			"en" => "Featured Article",
			"ta" => "சிறப்பு கட்டுரை"
		},
		"Image Of The Day" => {
			"en" => "Image Of The Day",
			"ta" => "இன்றைய சிறப்பு படம்"
		},
		"On This Day" => {
			"en" => "On This Day",
			"ta" => "இன்றைய நாளில்"
		},
		"News" => {
			"en" => "News",
			"ta" => "செய்திகள்" 
		}
	}


	PAGE_NOT_FOUND_MESSAGE = {
		"en" => "Sorry, There is no article under this name." ,
		"ta" => "மன்னிக்கவும், அப்படி ஒரு கட்டுரை இல்லை."
	}
	NO_MOST_READ_CONTENT_MESSAGE = {
		"en" => "Sorry, There is no most read contents available right now.",
		"ta" => "மன்னிக்கவும், இப்போது மிக அதிகமான வாசிப்பு உள்ளடக்கங்கள் இல்லை."
	}
	CANT_UNDERSTAND_MESSAGE = {
		"en" => "Sorry, I couldn't understand that.",
		"ta" => "மன்னிக்கவும், எனக்கு புரியவில்லை."
	}
	CANT_LOAD_OLD_NEWS_MESSAGE = {
		"en" => "Sorry, Couldn't load old news",
		"ta" => "மன்னிக்கவும், பழைய செய்தி கிடைக்கவில்லை"
	}
	READ_MORE_BUTTON = {
		"en" => "Read More",
		"ta" => "மேலும் வாசிக்க"
	}

	VIEW_ON_BROWSER_BUTTON = {
		"en" => "View on Browser",
		"ta" => "உலாவியில் பார்க்கவும்"
	}

	GET_SUMMARY_BUTTON = {
		"en" => "Get Summary",
		"ta" => "நிகழ்ச்சி சுருக்கம்"
	}

	FEATURED = {
		"en" => "Featured",
		"ta" => "சிறப்பு கட்டுரைகள்"
	}

	MOST_READ_MESSAGE = {
		"en" => "Most Read",
		"ta" => "அதிகம் வாசித்தது"
	}

	ON_THIS_DAY_MESSAGE ={
		"en" => "On This Day",
		"ta" => "இன்றைய நாளில்"
	}
	FEATURED_ARTICLE_MESSAGE = {
		"en" => "Featured Article",
		"ta" => "சிறப்பு கட்டுரை"
	}
	IMAGE_OF_THE_DAY_MESSAGE = {
		"en" => "Image Of The Day",
		"ta" => "இன்றைய சிறப்பு படம்"
	}
	NEWS_MESSAGE = {
		"en" => "News",
		"ta" => "செய்திகள்"
	}
	RANDOM_ARTICLE_MESSAGE = {
		"en" => "Random Article",
		"ta" => "ஏதாவது ஒரு கட்டுரை"
 	}

 	SUBSCRIPTION_BUTTON = {
 		"en" => "My Subscriptions",
 		"ta" => "என் சந்தாக்கள்"
 	}

 	SUBSCRIBE_BUTTON = {
 		"en" => "Subscribe",
 		"ta" => "பதிவு"
 	}

 	UNSUBCRIBE_BUTTON ={
 		"en" => "Unsubscribe",
 		"ta" => "குழுவிலகல்"
 	}

 	HELP_BUTTON = {
 		"en" => "Help",
 		"ta" => "உதவி"
 	}

 	MORE_BUTTON = {
 		"en" => "More",
 		"ta" => "மேலும்"
 	}


 	LANGUAGE_SETTINGS_BUTTON = {
 		"en" => "Language Settings",
 		"ta" => "மொழி அமைப்புகள்"
 	}

end